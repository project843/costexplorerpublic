from flask import Flask, render_template,request,jsonify
import boto3
import os
import boto3
import pandas as pd
import numpy as np
import re
import sys
import datetime
import json
import plotly
import plotly.express as px
app = Flask(__name__,template_folder='template',static_folder='staticFiles')

################## FUNCTIONS ##################"
def createDict(filterType,key,values):
    Dict = {}
    Dict['Key'] = key
    Dict['Values'] = values
    return { filterType:Dict }

def AND(*Dict):
    return { "And" : [ elt for elt in Dict] }

def OR(*Dict):
    return { "Or" : [ elt for elt in Dict] }

def NOT(Dict):
    return { "Not" :Dict}

def groupBy(Type,key):
    #Type : 'DIMENSION'|'TAG'|'COST_CATEGORY'
    return {'Type': Type ,'Key':key}

def checkDimensions (dimension):
    assert type(dimension) is dict, "The dimension ({}) that you entered is invalid. A dimension must be a dictionnary.".format(dimension)
    root= list(dimension.keys())[0]
    assert type(root) is str ,  " The root of this dimensions ({}) must be a string.".format(dimension)
    assert root in("And", "Or", "Not", "Dimensions", "Tags") , "Please select valid dimension root: 'And', 'Or', 'Not', 'Dimensions', 'Tags' for {}.".format(dimension)
    assert all(item in ["Key","Values"] for item in list(dimension[root].keys()))  , "The dimension ( {} ) is invalid. A dimension must have 2 key values :  'Key','Values'".format(dimension)
    assert type(dimension[root]["Key"]) is str ,  "The key of this dimensions ({}) must be a string.".format(dimension)
    assert type(dimension[root]["Values"]) is list , "The values of this dimensions ({}) must be a list.".format(dimension)
    assert dimension[root]["Values"] , "The list of values of this dimensions ({}) is empty. Please enter at least one value.".format(dimension)
    for value in dimension[root]["Values"]:
        assert type(value) is str ,  "The value(s) of this dimensions ({}) must be string(s).".format(dimension)
        
    if root == "Dimensions":
        assert dimension[root]["Key"] in ( 'AZ', 'REGION', 'INSTANCE_TYPE' ,'SERVICE','USAGE_TYPE_GROUP','RESOURCE_ID') , "The key of this dimensions ({}) is not valid. It must be :  'AZ', 'REGION', 'INSTANCE_TYPE' ,'SERVICE','USAGE_TYPE_GROUP','RESOURCE_ID'.".format(dimension)
      
    elif root == "Tags":
        assert dimension[root]["Key"] != "" , "The key of this dimensions ({}) is empty. Please enter a key.".format(dimension)

    elif root == "Not":
        assert len(list(dimension[root].keys())) ==1 , " This dimensions ({}) doesn't accept more than one operand.".format(dimension)
        checkDimensions(dimension[root])
         
    else:
        assert type(dimension[root]) is list , " The values of this dimensions ({}) must be a list.".format(dimension)
        assert len(dimension[root]) >=2 , " This dimensions ({}) must have at least two values".format(dimension)
        
        dimensionList = [ elt for elt in dimension[root] ]
        for elt in dimensionList:
            checkDimensions(elt)
            


@app.route('/')
def home():
   return render_template('ui.html')






@app.route('/instances' ,methods=['GET'])
def get_instances():
    client = boto3.client(
         "ec2",
        aws_access_key_id="AKIAXHY4XW3Y6SPUE7K3",
        aws_secret_access_key="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",
        region_name='eu-west-2')
    data=client.describe_instances()
    instance_types = []
    for reservation in data['Reservations']:
     for instance in reservation['Instances']:
        instance_types.append(instance['InstanceType'])
        
    return {"instance_types": instance_types}


@app.route('/tags', methods=['GET'])
def get_tags():
        client = boto3.client(
        "ce",
        aws_access_key_id="AKIAXHY4XW3Y6SPUE7K3",
        aws_secret_access_key="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",
            )
        response = client.get_tags(TimePeriod={
                'Start': '2022-05-06',
                'End': '2022-06-30'
            })
        tags = response["Tags"]
        dictionnary_of_tags={}
        for tag in tags:
            response = client.get_tags(
                TagKey=tag,
                TimePeriod={
                'Start': '2022-05-06',
                'End': '2022-06-30'
            })
            dictionnary_of_tags[tag]=response["Tags"]
            
        return dictionnary_of_tags


@app.route('/test', methods=["GET",'POST'])
def test():
    json_request= request.json
    REQUEST = {}
    
    REQUEST["granularity"] = json_request["granularity"]
    REQUEST["metrics"]= json_request["metrics"]
    if json_request["groupby"] != "NONE":
        gby = groupBy("DIMENSION",json_request["groupby"])
        REQUEST["groupby"]= [gby]
    
    date = json_request["Date"]
    date = date.split(" - ")
    start_date = date[0]
    end_date = date[1]
        
    if json_request["granularity"] == "HOURLY" : 
        start_date = start_date.split(" ")
        start_date = start_date[0]+"T" +start_date[1]+ 'Z'
        end_date = end_date.split(" ")
        end_date = end_date[0]+"T" +end_date[1]+ 'Z'
    else:
        start_date = date[0]
        end_date = date[1]
        
    REQUEST["start_date"] = start_date
    REQUEST["end_date"] = end_date
    
    if len(json_request["tags"]) ==0 : 
        if len(json_request["dimensions"]) ==1:
            one_filter= json_request["dimensions"][0]
            dim = createDict ("Dimensions", one_filter["dimension"] , one_filter["options"] )
            REQUEST["filter"]="Dimensions"
            REQUEST["dimensions"] = [dim]
        if len(json_request["dimensions"]) > 1:
            REQUEST["filter"]="And"
            REQUEST["dimensions"] =[]
            for elt in json_request["dimensions"]:
                dim = createDict ("Dimensions", elt["dimension"] , elt["options"] )
                REQUEST["dimensions"].append(dim)
                
    if len(json_request["tags"]) !=0 :
        if len(json_request["dimensions"]) ==0:
                REQUEST["filter"]="Tags"
                if len(json_request["tags"]) == 1 :
                    tag = json_request["tags"][0]
                    dim = createDict ("Tags",tag["Key"], tag["Values"])
                    REQUEST["dimensions"] = [dim]
                else:
                    REQUEST["dimensions"] =[]
                    for elt in json_request["tags"]:
                        dim = createDict ("Tags",elt["Key"], elt["Values"])
                        REQUEST["dimensions"].append(dim)
        if len(json_request["dimensions"]) > 1:
            REQUEST["filter"]="And"
            REQUEST["dimensions"] =[]
            for elt in json_request["tags"]:
                dim = createDict ("Tags",elt["Key"], elt["Values"])
                REQUEST["dimensions"].append(dim)
            for elt in json_request["dimensions"]:
                dim = createDict ("Dimensions", elt["dimension"] , elt["options"] )
                REQUEST["dimensions"].append(dim)
            
    client = boto3.client(
        "ce",
        aws_access_key_id="AKIAXHY4XW3Y6SPUE7K3",
        aws_secret_access_key="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",
    )

            ########## EVALUATION #######
            
    dailyTimeExpression = re.compile('\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\dZ')
    monthlyTimeExpression =  re.compile('\d\d\d\d-\d\d-\d\d')
    errors = []
    
    
    # CHECK GRANULARITY
    try:
        assert REQUEST["granularity"]  in( "DAILY","MONTHLY","HOURLY") , "Please select a valid granularity format: MONTHLY, DAILY, HOURLY."
        USER_GRANULARITY = REQUEST["granularity"] 
    except AssertionError as e:
        errors.append(e)
    
        
    # CHECK DATE EXPRESSIONS       
    if  (REQUEST["granularity"]  == "HOURLY"):
        try:
            #Time : YYYY- MM-DDThh:mm:ssZ.
            assert (dailyTimeExpression.fullmatch(REQUEST["start_date"]) is not None) and (dailyTimeExpression.fullmatch(REQUEST["end_date"]) is not None), "Please enter the date in the correct format: yyyy-mm-ddThh:mm:ssZ"
    
            startDate = REQUEST["start_date"].split("T")[0].split("-")
            startTime = REQUEST["start_date"].split("T")[1].split(":")
            
            endDate= REQUEST["end_date"].split("T")[0].split("-")
            endTime= REQUEST["end_date"].split("T")[1].split(":")
            # Check if the start date is greater than the end date
            try :
                assert 1<int(startDate[1])<12 and 1<int(endDate[1])<12  , "Please enter a valid month : 1...12"
                assert 1<int(startDate[2])<31 and  1<int(endDate[2])<31 , "Please enter a valid day : 1...31"
                assert datetime.datetime(int(startDate[0]),int(startDate[1]), int( startDate[2]), int(startTime[0]),int(startTime[1]),int(startTime[2][:-1] )) <  datetime.datetime( int(endDate[0]),int(endDate[1]), int( endDate[2]), int(endTime[0]) ,int(endTime[1]) ,int(endTime[2][:-1]) ), "Please enter an end date greater than the start date."
                USER_START_DATE = REQUEST["start_date"]
                USER_END_DATE = REQUEST["end_date"]                                                                                     
            except (AssertionError , ValueError ) as e:
                    errors.append(e)
        except AssertionError as e:
            errors.append(e)
    
    if (REQUEST["granularity"]  == "MONTHLY" or REQUEST["granularity"]  == "DAILY") :
        try:
            #Time : YYYY- MM-DD 
         
            assert (monthlyTimeExpression.fullmatch(REQUEST["start_date"]) is not None) and (monthlyTimeExpression.fullmatch(REQUEST["end_date"]) is not None), "Please enter the date in the correct format: yyyy-mm-dd."       
            startDate = REQUEST["start_date"].split("T")[0].split("-")
            endDate= REQUEST["end_date"].split("T")[0].split("-")
            try :
                assert 1<int(startDate[1])<12 and 1<int(endDate[1])<12  , "Please enter a valid month : 1...12"
                assert 1<int(startDate[2])<31 and  1<int(endDate[2])<31 , "Please enter a valid day : 1...31"
                assert datetime.datetime(int(startDate[0]),int(startDate[1]), int( startDate[2])) <  datetime.datetime( int(endDate[0]),int(endDate[1]), int( endDate[2]) ), "Please enter an end date greater than the start date."
                USER_START_DATE = REQUEST["start_date"]
                USER_END_DATE = REQUEST["end_date"]                                                                                     
            except AssertionError as e:
                    errors.append(e)
        except AssertionError as e:
            errors.append(e)
    
            
    # CHECK METRICS    
    USER_METRIC= []
    for metric in REQUEST["metrics"]:
        try:
            assert metric  in(  'AmortizedCost' , 'BlendedCost' , 'NetAmortizedCost' , 'NetUnblendedCost' , 'NormalizedUsageAmount' , 'UnblendedCost' , 'UsageQuantity') , "The metric ({}) that you entered is not valid. Please select valid metrics: AmortizedCost , BlendedCost , NetAmortizedCost , NetUnblendedCost , NormalizedUsageAmount , UnblendedCost ,UsageQuantity. ".format(metric)
            USER_METRIC.append(metric)
        except AssertionError as e:
            errors.append(e)   
    
    
        
    if "filter" in REQUEST.keys() :    
            # CHECK DIMENSIONS
        try:
            assert type(REQUEST["dimensions"]) is list , "The dimensions must be a list.".format(REQUEST["filter"])
            for dim in REQUEST["dimensions"]:
                try:
                    checkDimensions(dim)
                except AssertionError as e:
                    errors.append(e)        
        except AssertionError as e:
            errors.append(e)  
    
        
        # CHECK THE FILTER 
        try:
            assert REQUEST["filter"]  in("And", "Or", "Not", "Dimensions", "Tags") , "Please select valid filter root: 'And', 'Or', 'Not', 'Dimensions', 'Tags'."
            USER_FILTERS={}
        except AssertionError as e:
            errors.append(e)        
    
        if REQUEST["filter"] =="And" or REQUEST["filter"] =="Or" :
            try:
                assert len(REQUEST["dimensions"]) >= 2 , "The filter ({}) must have at least 2 operands.".format(REQUEST["filter"])
                USER_FILTERS[REQUEST["filter"]]= REQUEST["dimensions"]
            except AssertionError as e:
                errors.append(e)  
    
    
        if REQUEST["filter"] =="Dimensions" or REQUEST["filter"] =="Tags" :
            try:
                assert len(REQUEST["dimensions"]) == 1 , "The filter ({}) doesn't accept more than 1 operand, choose the filter 'And' or 'Or' instead.".format(REQUEST["filter"])
                tempdim = REQUEST["dimensions"][0]
                rootdim = list(REQUEST["dimensions"][0].keys())[0]
                assert rootdim == REQUEST["filter"],"The filter you entered is not suitable with the dimension. Please choose the right filter ({}) for this dimension ({}).".format(rootdim,tempdim)
                USER_FILTERS[REQUEST["filter"]]= tempdim[rootdim]
            except AssertionError as e:
                errors.append(e)  
            
    #CHECK Groupby
    if "groupby" in REQUEST.keys() :  
        try:
            assert type(REQUEST["groupby"]) is list, " GroupBy must be a list of dimensions"
            assert len(REQUEST["groupby"]) <= 2, " GroupBy can accept up to 2 dimensions"
            assert len(REQUEST["groupby"]) > 0, "GroupBy : please enter at least one dimension"
            for dim in REQUEST["groupby"]:
                assert all(item in ["Type","Key"] for item in list(dim.keys()))  , "The groupBy option ( {} ) is invalid. It must have 2 key values : 'Type', 'Key'".format(dim)
                assert dim["Type"] in('DIMENSION','TAG','COST_CATEGORY') , "Please select valid type for the groupby option: 'DIMENSION','TAG','COST_CATEGORY' for ({}).".format(dim)
                assert type(dim["Key"]) is str , "The groupBy option ( {} ) has an invalid Key. The key must be a string".format(dim)
               # if dim["Type"]  =='DIMENSION':
                   # assert dim["Key"] in ( 'AZ' , 'INSTANCE_TYPE' , 'LEGAL_ENTITY_NAME' , 'INVOICING_ENTITY' , 'LINKED_ACCOUNT' , 'OPERATION' , 'PLATFORM' , 'PURCHASE_TYPE' , 'SERVICE' , 'TENANCY' , 'RECORD_TYPE', 'USAGE_TYPE','REGION','ResourceId') , " The  groupby dimension ({}) has an invalid key. The accepted keys are: 'AZ' , 'INSTANCE_TYPE' , 'LEGAL_ENTITY_NAME' , 'INVOICING_ENTITY' , 'LINKED_ACCOUNT' , 'OPERATION' , 'PLATFORM' , 'PURCHASE_TYPE' , 'SERVICE' , 'TENANCY' , 'RECORD_TYPE', 'USAGE_TYPE'".format(dim)
        except AssertionError as e:
            errors.append(e)  
            
                    
    print(REQUEST)
            
        
         ############ SENDING REQUEST ################

    print("..Attemtpting to retreive data for the request..")

    if not errors:
            if "filter" in REQUEST.keys() and "groupby" in REQUEST.keys() :  
                
                response = client.get_cost_and_usage(
                        TimePeriod={
                            "Start":USER_START_DATE,
                            "End": USER_END_DATE
                        },
                        Granularity=USER_GRANULARITY,
                        Filter= USER_FILTERS,
                        Metrics=USER_METRIC,
                        GroupBy=REQUEST["groupby"]
                    )
            
            elif "filter" not in REQUEST.keys() and "groupby" not in REQUEST.keys() :  
            
                response = client.get_cost_and_usage(
                        TimePeriod={
                            "Start":USER_START_DATE,
                            "End": USER_END_DATE
                        },
                        Granularity=USER_GRANULARITY,
                        Metrics=USER_METRIC,
                    )
            
            elif "filter" in REQUEST.keys() and "groupby" not in REQUEST.keys() :  
            
                response = client.get_cost_and_usage(
                        TimePeriod={
                            "Start":USER_START_DATE,
                            "End": USER_END_DATE
                        },
                        Granularity=USER_GRANULARITY,
                        Metrics=USER_METRIC,
                        Filter= USER_FILTERS,
                    )
            else :
                
                response = client.get_cost_and_usage(
                        TimePeriod={
                            "Start":USER_START_DATE,
                            "End": USER_END_DATE
                        },
                        Granularity=USER_GRANULARITY,
                        Metrics=USER_METRIC,
                         GroupBy=REQUEST["groupby"]
                    )
    
            
            print('Data retrieved successfully!')
            print("")
            d = {}

            d["date"]=[]
            for metric in REQUEST["metrics"]:
                d[metric] = []
                
            if "groupby" in REQUEST.keys() :
                groupby = REQUEST["groupby"][0]["Key"]
                d[groupby] = []
                for entry in response["ResultsByTime"] : 
                    if entry["Groups"]:
                        for grp in entry["Groups"] :
                            d["date"].append(entry["TimePeriod"]["Start"])
                            d[groupby].append(grp["Keys"][0])
                            for metric in grp["Metrics"].keys() :
                                d[metric].append(grp["Metrics"][metric]['Amount'])
                    else:
                        d["date"].append(entry["TimePeriod"]["Start"])
                        d[groupby].append("N/A")
                        for metric in entry["Total"].keys() :
                                d[metric].append(entry["Total"][metric]["Amount"])
            
            
            else: 
                for entry in response["ResultsByTime"] : 
                    d["date"].append(entry["TimePeriod"]["Start"])
                    for metric in entry["Total"].keys() :
                        d[metric].append(entry["Total"][metric]["Amount"])
            
            
            
            df = pd.DataFrame(data=d)
            for metric in REQUEST["metrics"]:
                df[metric] = df[metric].astype(float)
                
            if "groupby" in REQUEST.keys() :
                fig = px.bar(df, x="date", y=REQUEST["metrics"][0], color=groupby,width=1400, height=800,
                             labels={'date':'Date', REQUEST["metrics"][0]:'Cost in USD'})
               
            else:
                fig = px.bar(df, x="date", y=REQUEST["metrics"][0],width=1400, height=800,
                             labels={'date':'Date', REQUEST["metrics"][0]:'Cost in USD'})
                
                
            fig.write_html(r"C:\Users\maxernsts\Desktop\UI\template\plot.html")    
            response= {}
            response["date"] = list(df["date"])
         
            if "groupby" in REQUEST.keys():
                gboption= json_request["groupby"]
                response["groupby"]= {gboption: list(df[gboption])}
            if "metrics" in REQUEST.keys():
                metric = REQUEST["metrics"][0]
                response["metrics"]= {metric: list(df[metric])}
                
                
            print((response))
            response= plotly.io.to_html(fig)
            return json.dumps(response, indent = 4)
    
    
    else:
        print('\033[1;31;4m')
        print("Attempt failed!")
        for err in errors:
            print('\033[1;31;4m')
            print(err)
        return{"ERRORS"}
            
       


if __name__ == '__main__':
   app.run(debug=True,use_reloader=False,threaded=True)